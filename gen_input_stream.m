% define variables
alphabet = 'ABCD';
stream_len = 1e3*2;
pattern_len = 15;

% initialization
alfabet_size = length(alphabet);
char_length = ceil(log2(alfabet_size));
fid1 = fopen('input_stream.txt','w');
fid2 = fopen('pattern.txt','w');

% build stream files
rand_stream = randi(alfabet_size, [stream_len,1]);
for i = 1:stream_len
    fprintf(fid1,'%s',alphabet(rand_stream(i)));
end
fclose(fid1);

% build pattern file 
rand_pattern = randi(alfabet_size, [pattern_len,1]);
for i = 1:pattern_len
    fprintf(fid2,'%s',alphabet(rand_pattern(i)));
end
fclose(fid2);