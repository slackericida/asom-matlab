% author: Filippo Maria Bianchi
% email: filippombianchi@google.com

classdef SeqRecognitionOnline<handle

properties
    mTol;
    mOccurrencies;
    mPos;
    mSequence;
    mHits;
end
    
methods (Access = public)
    
    % Constructor
    function obj=SeqRecognitionOnline(tol)

        obj.mTol = tol;
        obj.mOccurrencies = cell(tol+1,1);
        obj.mPos = 1;
        obj.mSequence = '';
        obj.mHits = cell(tol+1,1);
        
    end 
    
    % Main function
    function occs = Process(obj, input_seq, word )
    
        obj.mSequence = [obj.mSequence , input_seq];
        Lseq = length(obj.mSequence);
        Lword = length(word);
        Lmin = Lword - obj.mTol -1;
        Lmax = Lword + obj.mTol -1;
        

        % integrity check
        if(obj.mTol >= Lword)
            error('Tolerance value too high')
        end
        if(obj.mTol >= Lword)
            error('Tolerance value too high')
        end
        if(Lmax >= Lseq)
            error('Input sequence too short')
        end


        % browse the whole sequence looking for occurrences and stop as soon as the minimum length mask would end beyond the sequence
        while(obj.mPos <= Lseq - Lmax)

            % starting from the current position, try every mask
            for n=Lmin:Lmax

                % check if the mask ends after the end of the sequence
                if(obj.mPos + n <= Lseq)

                    % evaluate the dissimilarity of the subsequence from the prototype
                    subseq = obj.mSequence(obj.mPos:obj.mPos+n);
                    %diss = edit_distance_weighted(subseq,word,2,1,1);
                    diss = strdist(subseq,word);

                    % if the dissimilarity d of the subsequence from the prototype is < tol, save the subsequence in the container d
                    if(diss <= obj.mTol)
                        obj.mHits{diss+1} = [obj.mHits{diss+1}, [obj.mPos;n]];
                    end

                end

            end

            % as the current position pos advance, check if is it possible to return any of the subsequences found. A subsequence can be
            % returned if it started on a position sufficient far from pos and it has not been removed from Hits.
            for d=1:obj.mTol+1

                % check if the container relative to the distance 'd' contains at least one subsequence
                if(~isempty(obj.mHits{d}))

                    % select the first element (the next ones started in a later position)
                    subseq_d = obj.mHits{d}(:,1);

                    % check if subseq_d can be returned
                    if(subseq_d(1) == obj.mPos - (Lword*(d-1) + (d-1)*(d-2)/2 + Lword))

                        obj.mOccurrencies{d} = [obj.mOccurrencies{d}, subseq_d];
                        obj.mHits{d}(:,1) = [];

                        % remove all the subsequences with higher distance overlapping with subseq_d
                        for d2 = d:obj.mTol+1

                            i=1;
                            while(i <= size( obj.mHits{d2}, 2) )

                                subseq_d2 = obj.mHits{d2}(:,i);

                                % check if is it possible to remove the subsequence subseq_i (if it is overlapping with subseq_d)
                                if(~((subseq_d(1) + subseq_d(2) <= subseq_d2(1)) || (subseq_d2(1) + subseq_d2(2) <= subseq_d(1))))
                                    obj.mHits{d2}(:,i) = [];
                                else
                                    i = i+1;
                                end
                            end
                        end

                    end

                end

            end

            obj.mPos = obj.mPos +1;

        end
           
        occs = obj.mOccurrencies;

    end % end Process function
    
    % return what is left in Hits
    function [occs] = EmptyAll(obj)
      
        for d = 1:obj.mTol+1

            if(~isempty(obj.mHits{d}))

                ind = 1;
                while(ind <= size(obj.mHits{d},2))
                    % return the subsequence subseq_d with minor distance from the pattern, among the ones left in Hits
                    subseq_d = obj.mHits{d}(:,ind);
                    obj.mOccurrencies{d} = [obj.mOccurrencies{d}, subseq_d];
                    obj.mHits{d}(:,ind) = [];

                    % remove all the subsequences in Hits, with a distance greater than subseq_d and overlapping with it
                    for d2 = d:obj.mTol+1

                        ind2 = ind;
                        while(ind2 <= size(obj.mHits{d2},2) )

                            subseq_d2 = obj.mHits{d2}(:,ind2);

                            if(~((subseq_d(1) + subseq_d(2) <= subseq_d2(1)) || (subseq_d2(1) + subseq_d2(2) <= subseq_d(1))))
                                obj.mHits{d2}(:,ind2) = [];
                            else
                                ind2 = ind2 + 1;
                            end                  
                        end
                    end   
                end
            end    
        end
        
        occs = obj.mOccurrencies;
        
    end
    
end % end public methods

end % end class




