clear all

seq = fileread('input_stream_symb.txt');
pattern = fileread('pattern.txt');


fprintf('--- ASOM ---\n');
start_time = cputime;
Nseq = length(seq);
input_step = 500;
n_inputs = floor(Nseq / input_step);
seq_rec = SeqRecognitionOnline(2);
for i = 1:input_step:n_inputs*input_step
    seq_i = seq(i:i+input_step-1);
    seq_rec.Process( seq_i, pattern );
end

occurrencies = seq_rec.EmptyAll();
tot_occ = 0;
for i=1:length(occurrencies)
    occ_i = occurrencies{i};
    tot_occ = tot_occ+size(occ_i,2);
    for j=1:size(occ_i,2)
        disp(['dist ',num2str(i-1),' -- pos: (',num2str(occ_i(1,j)),',',num2str(occ_i(1,j) + occ_i(2,j)),') - ',seq(occ_i(1,j):occ_i(1,j)+occ_i(2,j))])
    end
end
fprintf('\n')
end_time = cputime-start_time;
disp(['tot time: ',num2str(end_time),'. Occurrences found: ',num2str(tot_occ)])
