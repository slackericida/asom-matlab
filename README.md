# README #

MATLAB implementation of the Approximate String Online Matching (ASOM) algorithm. Given a stream **t**, a pattern **p** to be searched and a tolerance value *k*, the output is a set of non-overlapping subsequences **s** of **t**, so that the Levenshtein distance *d*(**s**,**p**) <= *k*. 
The algorithm is also available in a [C++ implementation](https://bitbucket.org/slackericida/asom-c/overview).

### Execution ###
To run a demonstration of the algorithm, first generate a test input stream **t** and a test pattern **p** using the function "gen_input_stream.m". The function will generate two text files, namely "input_stream.txt" and "pattern.txt", which contain
the sequence stream of characters and the pattern to be found. The alphabet, i.e. the symbols to be used in **t**, must be declared, as well ass the length of **t** and **p**. The following initialization generates a random stream of 200000 characters and a random pattern of length 15, using 4 symbols {A,B,C,D}:

```
#!matlab
alphabet = 'ABCD';
stream_len = 1e5*2;
pattern_len = 15;
```

Successively, run the script "Main.m", which run an example of the algorithm, searching in **t** all the occurrences **s** with a Levenshtein distance lower than *k*. To set the tolerance value *k* to a desired threshold, modify the value of the constructor. For example, 

```
#!matlab
seq_rec = SeqRecognitionOnline(2);
```

call the procedure using *k*=2 as tolerance threshold. The output returned is the set of the occurrences found, defined as a cell of pairs that contain the initial position in **t** and the length of the occurrence. The total number of occurrences found and the total time elapsed is displayed.

### Related literature ###

A pre-print of the article relative to the algorithm can be found at the [following link](https://google.com). Please, refer to it for a detailed description and explanation of the procedure.

### Contact Information ###

* Filippo Maria Bianchi
* E-mail: filippombianchi@gmail.com